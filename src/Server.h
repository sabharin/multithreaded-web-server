
#ifndef SERVER_H_
#define SERVER_H_

# include <ctype.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>
# include <string.h>
# include <limits.h>
# include <errno.h>
# include <time.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#ifndef MAXCONN
#define MAXCONN 10
#endif

#ifndef BUFFSIZE 
#define BUFFSIZE 1024 
#endif

int sockfd,newsock;

void startServer(int *portnum);
char* getCurrentTimeAsStr();

#endif
