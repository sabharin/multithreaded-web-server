# include <ctype.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <limits.h>
# include <errno.h>
# include <signal.h>
#include <sys/stat.h>
# define MAX_LENGTH 260
# include "myhttpd.h"
# include "Server.h"
# include "RequestProcessor.h"

pthread_mutex_t reqQueueLock;
pthread_mutex_t reqProcessorLock;
pthread_mutex_t loggerLock;
pthread_cond_t nextRequestCond;
pthread_cond_t queueCond;
sem_t threadSem;
bool debug = 0;
bool help = 0;
bool logging = 0;
FILE *logFile;
char *loggerFile = "requests.log";
int port = 8080;
bool dir = 0;
char *rootDir = NULL;
int queueTime = 60;
int numThreads = 4;
int option = 0;
int schedulerType = 0;

bool convertStringToNum(int *value, char *arg) {

	char *end;

	const long sl = strtol(arg, &end, 10);

	if (end == arg) {
		printf("%s: not a decimal number\n", arg);
	} else if ('\0' != *end) {
		printf("%s: extra characters at end of input: %s\n", arg, end);
	} else if ((LONG_MIN == sl || LONG_MAX == sl) && ERANGE == errno) {
		printf("%s out of range of type long\n", arg);
	} else if (sl > INT_MAX) {
		printf("%ld greater than INT_MAX\n", sl);
	} else if (sl < INT_MIN) {
		printf("%ld less than INT_MIN\n", sl);
	} else {
		*value = (int) sl;
		printf("Converted Value is %d \n", *value);
		end = NULL;
		return true;
	}
	end = NULL;
	return false;
}

void printHelp() {
	printf(
			"\n ------------------------------------------------------------------------------- \n");
	printf(
			"Usage Summary: myhttpd [-d] [-h] [-l file] [-p port] [-r dir] [-t time] [-n threadnum] [-s sched] \n");
	printf(
			" -d : It will run the Web Server in debug mode and in Single Threaded environment\n");
	printf(" -h : Print a usage summary with all options and exit \n");
	printf(
			" -l file : Log all requests to the given file. For example: logging.txt \n");
	printf(
			" -p port : Listen on the given port. If not provided, myhttpd will listen on port 8080. For example: -p 8181 \n");
	printf(
			" -r root : Set the root directory for the http server to dir. For example: -r home/rootdirectory/Myhttpd/ \n");
	printf(
			" -t time : Set the queuing time to time seconds. The default should be 60 seconds. For example: -t 45 \n");
	printf(
			" -n threadnum: Set number of threads waiting ready in the execution thread pool to threadnum. The default should be 4 execution threads. For example: -n 10 \n");
	printf(
			" Set the scheduling policy. It can be either FCFS or SJF. The default will be FCFS. for example: -s SJF \n");
	printf(" Press Ctrl+c anytime to exit the server\n");
	printf(
			" ------------------------------------------------------------------------------- \n");
	exit(1);
}

void signalHandler(int signum) {
	printf("Signal handler is called \n");
	close(sockfd);
	exit(signum);
}

int main(int argc, char **argv) {
	int c = 0;
	int i;
	opterr = 0;

	for (i = 0; i < argc; i++) {
		printf(" argv :: %s \n", argv[i]);
	}

	while ((c = getopt(argc, argv, "dhl:p:r:t:n:s:")) != -1) {
		switch (c) {
		case 'd':
			option |= 1;
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %s \n",
					opterr, optopt, optind, optarg);
			break;
		case 'h':
			option |= 2;
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %d \n",
					opterr, optopt, optind, help);
			break;
		case 'l':
			option |= 4;
			loggerFile = malloc(strlen(optarg) + 1);
			if (loggerFile == NULL) {
				return 0;
			}
			strcpy(loggerFile, optarg);
			logging = 1;
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %s \n",
					opterr, optopt, optind, loggerFile);
			break;

		case 'p':
			option |= 8;
			if (convertStringToNum(&port, optarg) == true) {
				if (port < 1024) {
					printf(
							"Cannot allow a privileged port to be used.. Setting to default port number");
					port = 8080;
				}
			} else {
				printf(
						"Port Number value is not string value.. Hence setting value ot default 8080\n");
				port = 8080;
			}
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %d \n",
					opterr, optopt, optind, port);
			break;
		case 'r':
			option |= 16;
			rootDir = malloc(strlen(optarg) + 1);
			if (rootDir == NULL) {
				return 0;
			}
			strcpy(rootDir, optarg);
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %s \n",
					opterr, optopt, optind, rootDir);
			break;

		case 't':
			option |= 32;
			if (convertStringToNum(&queueTime, optarg) == true) {
			} else {
				printf(
						"QueueTime value is not string value.. Hence setting value ot default 60 seconds \n");
				queueTime = 60;
			}

			if (queueTime < 0) {
				queueTime = 60;
			}
			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %d \n",
					opterr, optopt, optind, queueTime);
			break;

		case 'n':
			option |= 64;
			if (convertStringToNum(&numThreads, optarg)) {
			} else {
				printf(
						"QueueTime value is not string value.. Hence setting value ot default 4 Threads \n");
				numThreads = 4;
			}

			if (numThreads < 1) {
				numThreads = 4;
			}

			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %d \n",
					opterr, optopt, optind, numThreads);
			break;

		case 's':
			option |= 128;
			if (strcasecmp(optarg, "FCFS") == 0) {
				schedulerType = 0;
			} else if (strcasecmp(optarg, "SJF") == 0) {
				schedulerType = 1;
			} else {
				printf(
						"Not a proper Scheduler Type.. Using Default Algorithm FCFS \n");
				schedulerType = 0;
			}

			printf("opterr:: %d , optopt:: %d, optind :: %d, optarg:: %d \n",
					opterr, optopt, optind, schedulerType);
			break;

		case '?':
			if (optopt == 'l' || optopt == 'p' || optopt == 'r' || optopt == 't'
					|| optopt == 'n' || optopt == 's')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		default:
			abort();
		}
	}

	printf("Root Dir setting \n");
	if ((option & 16) == 16) {
		if (chdir(rootDir) < 0) {
			perror("\n Directory doesnt exist");
			return 0;
		}
	}else{
		printf("Root Directory Setting \n");
		rootDir = (char *)malloc(PATH_MAX);
		rootDir = getcwd(rootDir,PATH_MAX);
	}

	printf("Root Dir %s\n",rootDir+strlen(rootDir)-1);
	if(strcmp((rootDir+strlen(rootDir)-1) , "/") != 0){
		printf("appending / to rootDir");
		rootDir = (char *)realloc(rootDir,strlen(rootDir)+1);
		strcat(rootDir,"/");
	}
	printf("Root Dir %s\n",rootDir);

	if(logging && !debug){
		char *fullPath = (char *)malloc(strlen(rootDir)+strlen(loggerFile));
		strcpy(fullPath,rootDir);
		strcat(fullPath,loggerFile);
		free(loggerFile);
		loggerFile = (char *)malloc(strlen(fullPath));
		strcpy(loggerFile,fullPath);
		free(fullPath);
	}

	if ((option & 1) == 1) {
		numThreads = 1;
		logging = true;
	} else{
		daemon(0,0);
	}

	if ((option & 2) == 2) {
		printHelp();
		return 0;
	}

	printf("start server function is to be called");

	pthread_t threadProcessor[numThreads];
	pthread_t schedulerThread, serverThread;


	if (pthread_mutex_init(&reqQueueLock, NULL) != 0) {
		printf("mutex init failed\n");
		return 1;
	}

	if (pthread_mutex_init(&reqProcessorLock, NULL) != 0) {
		printf("mutex init failed\n");
		return 1;

	}

	if (pthread_cond_init(&nextRequestCond, NULL) != 0) {
		printf("Cond init failed\n");
		return 1;
	}

	if (pthread_cond_init(&queueCond, NULL) != 0) {
		printf("Cond init failed \n");
		return 1;
	}

	if (pthread_mutex_init(&loggerLock, NULL) != 0) {
		printf("Mutex init failed \n");
		return 1;
	}

	if (sem_init(&threadSem, 0, 0) != 0) {
		printf("Semaphore init failed \n");
		return 1;
	}

	pthread_create(&serverThread, NULL, &startServer, &port);

	printf("Server Started successfully \n");

	for (i = 0; i < numThreads; i++) {
		pthread_create(&threadProcessor[i], NULL, &runner, NULL);
	}

	sleep(queueTime);

	pthread_create(&schedulerThread, NULL, &scheduler, NULL);

	printf("Runner threads created \n");
	printf("Server Scheduler running \n");

	pthread_join(schedulerThread, NULL);

	for (i = 0; i < numThreads; i++) {
		pthread_join(&threadProcessor[i], NULL);
	}

	pthread_join(serverThread, NULL);

	pthread_cond_destroy(&queueCond);
	pthread_cond_destroy(&nextRequestCond);
	pthread_mutex_destroy(&reqQueueLock);
	pthread_mutex_destroy(&reqProcessorLock);
	pthread_mutex_destroy(&loggerLock);
	pthread_exit(NULL);

	printf("Exiting");

	close(sockfd);
	return 0;
}
