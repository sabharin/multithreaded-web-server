
#ifndef REQUESTPROCESSOR_H_
#define REQUESTPROCESSOR_H_

# include <stdio.h>
# include <stdlib.h>
# include <ctype.h>
# include <sys/fcntl.h>
# include <string.h>
#include <sys/types.h>
#include <dirent.h>

#ifndef BUFFSIZE
#define BUFFSIZE 1024
#endif

#define SUCCESS_TEXT	"HTTP/1.1 200 OK\n"
#define CONTENT_TEXT	"Content-Type:text/html\n"
#define CONTENT_IMAGE	"Content-Type:image/jpeg\n"
#define LAST_MODIFIED	"Last-Modified:"
#define CONTENT_LENGTH	"Content-Length:"
#define FAILURE_TEXT	"HTTP/1.1 404 Not Found\n"
#define FAILURE_CONTENT	"<html><body><h1>FILE NOT FOUND</h1></body></html>"
#define UNABLE_TO_HANDLE_REQUEST "<html><body><h1>UNABLE TO HANDLE REQUEST</h1></body></html>"

typedef struct node Element;

void processRequest(int acceptfd,char* ipAddress,char* incomingBuffer,char* arrivalTime);

void scheduler();

void getFirstRequest();

void runner();

#endif
