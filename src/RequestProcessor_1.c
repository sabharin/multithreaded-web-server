# include "myhttpd.h"
# include "RequestProcessor.h"
# include <pwd.h>
#include <stdio.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/stat.h>

struct request {
	int acceptfd;
	int fileSize;
	char* ipAddress;
	char* reqType;
	char* fileName;
	char* fullPath;
	bool isRootDir;
	char* arrivalTime;
	char *processedTime;
	char* firstLine;
	int status;
	int responseSize;
	time_t modTime;
};

struct node {
	struct request req;
	struct node *next;
}*head = NULL, *tail = NULL, *nextRequest = NULL;

static int threadid = 0;

char incomingBuffer[BUFFSIZE];

char tempBuf[BUFFSIZE];

void insertToQueue(Element *elem);

void print();

bool checkRootFolder(char* fileName);

char* getFullPath(char *buffer);

void insertRequest(int acceptfd, char* ipAddress, char* reqType, char* fileName,
		char* firstLine, char* arrivalTime);

char* getFileExtension(const char* fileName);

void processRequest(int acceptfd, char* ipAddress, char* incomingBuffer1,
		char* arrivalTime) {

//	printf("incoming buffer %s \n", tempBuf);

	int retcode = 0;
//	printf("Size of incoming buffer %ld \n", sizeof(tempBuf));
	memset(incomingBuffer, 0, sizeof(tempBuf));
	if ((retcode = recv(acceptfd, tempBuf, sizeof(tempBuf), 0)) < 0) {
		perror("Receive Error");
	}

//	printf("incoming buffer %s \n", tempBuf);

	strcpy(incomingBuffer, tempBuf);

	char* firstLine = strtok(incomingBuffer, "\r\n");
//	printf("incoming buffer %s \n", incomingBuffer);

//	printf("First line %s \n", firstLine);

	int len = snprintf(NULL, 0, "%s", firstLine);
	char* elemFirstLine = (char *) malloc((len + 1) * sizeof(char));
	snprintf(elemFirstLine, (len + 1), "%s", firstLine);
//	printf("incoming buffer %s \n", incomingBuffer);

	char* firstToken = strtok(firstLine, " ");
//	printf("incoming buffer %s \n", incomingBuffer);

	len = snprintf(NULL, 0, "%s", firstToken);
	char* reqType = (char *) malloc((len + 1) * sizeof(char));
	snprintf(reqType, (len + 1), "%s", firstToken);

//	printf("Req Type %s \n", reqType);
//	printf("First line %s \n", firstLine);

	char* secondToken = strtok(NULL, " ");
	char* fileName = NULL;
	if(strcmp(secondToken,"/") != 0)
	{
		len = snprintf(NULL, 0, "%s", secondToken);
		fileName = (char *) malloc((len + 1) * sizeof(char));
		snprintf(fileName, (len + 1), "%s", secondToken);
	}else{
		fileName = (char *) malloc(11);
		strcpy(fileName,"/index.html");
	}

//	printf("File Name %s \n", fileName);

	insertRequest(acceptfd, ipAddress, reqType, fileName, elemFirstLine,
			arrivalTime);
//	printf("incoming buffer %s \n", incomingBuffer);

	firstLine = NULL;
	firstToken = NULL;
	secondToken = NULL;
}

void processFileInfo(char* fileName, Element *elem) {

	bool isRootFolder = checkRootFolder(fileName);
//	printf(" File Name %s \n", fileName);
	char *fileDup = (char *) malloc(strlen(fileName));
	fileDup = strdup(fileName);
	char *root = getFullPath(fileDup);
//	printf("%d \n", isRootFolder);
//	printf(" Root %s \n", root);
//	printf(" File Name %s \n", fileName);
	elem->req.isRootDir = isRootFolder;
	elem->req.fullPath = root;
	free(fileDup);
}

bool checkRootFolder(char* fileName) {

	char *dup = (char *) malloc(strlen(fileName));
	dup = strdup(fileName);
	char *pos = strchr(dup, '/');
	if (pos != NULL) {
		int firstOccurence = pos - dup + 1;

		*pos = strchr(dup, '/');

		if (pos == NULL) {
			free(dup);
			return true;
		}
	}
	free(dup);
	return false;
}

char *replaceStr(char *str, char *strToRep, char *userString) {
	char buffer[4096];
	char *newString = NULL;
	char *p;

	if (!(p = strstr(str, strToRep)))  // Is 'orig' even in 'str'?
		return str;

	strncpy(buffer, str, p - str); // Copy characters from 'str' start to 'orig' st$
	buffer[p - str] = '\0';

	sprintf(buffer + (p - str), "%s%s", userString, p + strlen(strToRep));

	newString = (char *) malloc(strlen(buffer));

	strcpy(newString, buffer);

	return newString;
}

char* getFullPath(char *buffer) {
	char *val = (char *) malloc(strlen(buffer));
	strcpy(val, buffer);
	char *pos = strchr(val, '~');

	char *newString = NULL;
	char *userRoot = NULL;
	char *fullFilePath = NULL;
	//printf("%d", (pos - val + 1));
	if ((pos != NULL) && ((pos - val + 1) == 2)) {
		newString = strtok(pos + 1, "/");

		struct passwd *userChange= getpwnam(newString);
		if(userChange != NULL){
			char* userHome = malloc(sizeof((userChange->pw_dir)));
			strcpy(userHome,userChange->pw_dir);
			userRoot = replaceStr("<user>/myhttpd/", "<user>", userHome);
		}else{
			printf("User does not exists \n");
			userRoot = replaceStr("/home/<user>/myhttpd/", "<user>", newString);
		}

		newString = strtok(NULL, "/");
		printf("new string is %s",newString);
		if(newString == NULL){
				char *indexPath = (char *)malloc(strlen(userRoot));
				strcpy(indexPath,userRoot);
				userRoot = (char *)realloc(userRoot,strlen(indexPath) + 10);
				strcpy(userRoot,indexPath);
				strcat(userRoot,"index.html");
		}
	} else {
		userRoot = (char *) malloc(strlen(rootDir) + 6);
		strcpy(userRoot, rootDir);
		newString = strtok(val, "/");
	}

	char *path = NULL;
	while (newString != NULL) {
		if (newString != NULL) {
			if (path == NULL) {
				path = (char *) malloc(strlen(newString));
				strcpy(path, newString);
			} else {
				path = (char *) realloc(path,
						strlen(path) + strlen("/") + strlen(newString));
				strcat(path, "/");
				strcat(path, newString);
			}
		}
		newString = strtok(NULL, "/");
	}

	if (path == NULL) {
		fullFilePath = (char *) malloc(strlen(userRoot));
		strcpy(fullFilePath, userRoot);
	} else {
		fullFilePath = (char *) malloc(strlen(userRoot) + strlen(path));
		strcpy(fullFilePath, userRoot);
		strcat(fullFilePath, path);
	}

	free(userRoot);
	free(path);
	free(val);

	return fullFilePath;
}

void insertRequest(int acceptfd, char* ipAddress, char* reqType, char* fileName,
		char* firstLine, char* arrivalTime) {

	Element* elem = (Element *) malloc(sizeof(struct node));

	elem->req.acceptfd = acceptfd;

	int len = snprintf(NULL, 0, "%s", ipAddress);
//	printf("Length of address %d \n", len);
	elem->req.ipAddress = (char *) malloc((len + 1) * sizeof(char));
	snprintf(elem->req.ipAddress, (len + 1), "%s", ipAddress);

	/*	len = snprintf(NULL,0,"%s",reqType);
	 printf("Length of reqType %d \n",len);
	 elem->req.reqType = (char *)malloc((len+1)*sizeof(char));
	 snprintf(elem->req.reqType,(len+1),"%s",reqType);
	 */

	elem->req.reqType = reqType;

	/*	len = snprintf(NULL,0,"%s",fileName);
	 printf("Length of fileName %d \n",len);
	 elem->req.fileName = (char *)malloc((len+1)*sizeof(char));
	 snprintf(elem->req.fileName,(len+1),"%s",fileName);
	 */

	elem->req.fileName = fileName;

//	printf("\n File after setting %s", elem->req.fileName);

	processFileInfo(fileName, elem);

	int filesize = 0;

	struct stat fileInfo;
	printf("Full path %s \n", elem->req.fullPath);
	if (stat(elem->req.fullPath, &fileInfo) == 0) {
		//	printf("stat loaded \n");
		filesize = fileInfo.st_size;
	}

//	printf("File Size %d \n", filesize);
	elem->req.modTime = fileInfo.st_mtime;
	//filesize = --fileVal;

	elem->req.fileSize = filesize;

	printf("Firstline %s \n", firstLine);
	/*	len = snprintf(NULL,0,"%s",firstLine);
	 printf("Length of firstLine %d \n",len);
	 elem->req.firstLine = (char *)malloc((len+1)*sizeof(char));
	 snprintf(elem->req.firstLine,(len+1),"%s",firstLine);
	 */

	elem->req.firstLine = firstLine;

///	len = snprintf(NULL, 0, "%s", arrivalTime);
//	printf("Length of arrivalTime %d \n",len);
//	elem->req.arrivalTime = (char *) malloc((len + 1) * sizeof(char));
//	snprintf(elem->req.arrivalTime, (len + 1), "%s", arrivalTime);

	elem->req.arrivalTime = arrivalTime;

	elem->next = NULL;
	printf(" Element to be inserted \n");
//	printf(
	//		"------------------------------------------------------------------------------------------------ \n");

	printf(
			" Acceptfd %d \n "/*, IpAddress %s \n, ReqType %s \n, FileName %s \n ,FileSize %d \n, FirstLine %s \n, ArrivalTime %s \n"*/,
			elem->req.acceptfd/*, elem->req.ipAddress, elem->req.reqType,
			 elem->req.fileName, elem->req.fileSize, elem->req.firstLine,
			 elem->req.arrivalTime*/);

	//printf(
	//	"------------------------------------------------------------------------------------------------ \n");
	// lock the queue

	pthread_mutex_lock(&reqQueueLock);
	printf("Inserting an element \n");
	insertToQueue(elem);
	printf("\n Signalling insertion \n");
	pthread_cond_signal(&queueCond);
	pthread_mutex_unlock(&reqQueueLock);
	// unlock the queue 
	// signal 

	print();
}

void insertToQueue(Element *elem) {

//	printf("\n Scheduler Type is %d\n", schedulerType);
	if (head == NULL) {
		head = elem;
		//	printf("\n Inserting at head head size %d \n", head->req.fileSize);
		//	printf("elems file size %d \n", elem->req.fileSize);
		tail = elem;
	} else {
		if (schedulerType == 0) {
			tail->next = elem;
			tail = elem;
		} else {
			// Iterate through the list till file size > elem's file size
			//		printf("head size %d \n", head->req.fileSize);
			//		printf("elems file size %d \n", elem->req.fileSize);

			Element* temp = NULL;
			temp = head;
			//		printf(" File Size %d \n", temp->req.fileSize);

			//		printf(" Inserting in queue by fileSize \n");

			int size = elem->req.fileSize;
			int currentSize = temp->req.fileSize;

//			printf(" Acceptfd %d \n , IpAddress %s \n, ReqType %s \n, FileName %s \n, FirstLine %s \n, ArrivalTime %s \n", temp->req.acceptfd , temp->req.ipAddress, temp->req.reqType , temp->req.fileName , temp->req.firstLine , temp->req.arrivalTime);
			//		printf("---------------------------------------------------------------------------------------------------- \n");

			//	printf("Size %d , Current Size %d , compare currentSize <= size %d \n",size,currentSize,(currentSize > size));
			//printf("Size %d \n",size);
			//		printf("CurrentSize %d \n",currentSize);

			if (currentSize > size) {
				//		printf("Current Size is greater \n");
				elem->next = head;
				head = elem;
				//		printf("Completed insertion");
			} else {
				if (temp->next != NULL) {
					while (true) {
						//	printf(" Acceptfd %d \n , IpAddress %s \n, ReqType %s \n, FileName %s \n, FirstLine %s \n, ArrivalTime %s \n", temp->next->req.acceptfd , temp->next->req.ipAddress, temp->next->req.reqType , temp->next->req.fileName , temp->next->req.firstLine , temp->next->req.arrivalTime);
						//	printf("---------------------------------------------------------------------------------------------------- \n");

						int currentSize = temp->next->req.fileSize;

						//					printf("Size %d , Current Size %d , compare currentSize <= size %d \n",size,currentSize,(currentSize<=size));

						//					printf("Size %d \n",size);
						//					printf("CurrentSize %d \n",currentSize);

						if (currentSize <= size) {
							//						printf("Moving to next value \n");
							if (temp->next->next == NULL) {
								//lastInsert = true;
								break;
							} else {
								temp = temp->next;
							}
							//						printf("Moved temp");
						} else {
							break;
						}
					}

					//				printf("Identified location");
					if (temp->next->next == NULL) {
						temp->next->next = elem;
					} else {
						elem->next = temp->next;
						temp->next = elem;
					}

				} else {
					temp->next = elem;
				}
			}
		}
	}
}

void scheduler() {
	printf("Scheduler Execution started \n");
	while (true) {
		printf("Waiting for threadSemaphore \n");

		sem_wait(&threadSem);
		pthread_mutex_lock(&reqQueueLock);
		while (head == NULL) {
			printf("Waiting for user request \n");
			pthread_cond_wait(&queueCond, &reqQueueLock);
		}

		pthread_mutex_lock(&reqProcessorLock);
		printf("Retrieving First Request \n");
		getFirstRequest();
		printf("Retrieved the first Request \n");
		pthread_cond_signal(&nextRequestCond);
		pthread_mutex_unlock(&reqProcessorLock);
		pthread_mutex_unlock(&reqQueueLock);
		while (nextRequest != NULL) {
			sleep(1);
		}
	}
}

void getFirstRequest() {
	nextRequest = head;
	head = head->next;
}

void runner() {
	int a = ++threadid;
	sem_post(&threadSem);
	while (true) {

		// Lock for multiple threads
		pthread_mutex_lock(&reqProcessorLock);

		// Wait for request to be filled
		printf("Thread id %d \n", a);
		printf("Waiting for Scheduler \n");
		pthread_cond_wait(&nextRequestCond, &reqProcessorLock);
		printf("Request Available!!!! \n");
		Element* currentRequest = nextRequest;

		nextRequest = NULL;

		pthread_mutex_unlock(&reqProcessorLock);

		printf("Processing the request acceptfd %d \n",
				currentRequest->req.acceptfd);

		currentRequest->req.processedTime = getCurrentTimeAsStr();
		time_t currTime;
		time(&currTime);
		struct tm *now = (struct tm*) gmtime(&currTime);
		currentRequest->req.processedTime = (char *) malloc(29 * sizeof(char));
		strftime(currentRequest->req.processedTime, 29,
				"[%d/%b/%Y:%H:%M:%S %z]", now);
		//	printf("Processed Time %s\n ",currentRequest->req.processedTime);
		processAndSendData(currentRequest);
		pthread_mutex_lock(&loggerLock);
		logCurrentRequest(currentRequest);
		pthread_mutex_unlock(&loggerLock);
		free(currentRequest);
		printf("the request acceptfd %d \n", currentRequest->req.acceptfd);
		printf("clearing current request \n");
		// Unlock for threads
		sem_post(&threadSem);
		printf("A thread is free now");
	}
}

void print() {

	Element* temp = NULL;
	if (head == NULL) {
		printf("Queue is empty !!\n");
	} else {
		temp = head;
		printf("Queue Print \n");
		while (temp != NULL) {
			printf(" Acceptfd %d \t File Size %d \n",/* , IpAddress %s \n, ReqType %s \n, FileName %s \n, FirstLine %s \n, ArrivalTime %s \n", */
			temp->req.acceptfd,
					temp->req.fileSize /*, temp->req.ipAddress, temp->req.reqType , temp->req.fileName , temp->req.firstLine , temp->req.arrivalTime*/);
			//	printf(
			//		"----------------------------------------------------------------------------------------------------");
			temp = temp->next;
		}
	}
}

void processAndSendData(Element* elem) {

	//bool fileExists = false;
	//printf("Entering Process and send Data \n");
	FILE* f = fopen(elem->req.fullPath, "r");
	//int fd1 = open(elem->req.fullPath, O_RDONLY, S_IREAD | S_IWRITE);
	char displayBuffer[BUFFSIZE];
	//printf("Memset to be done \n");
	memset(displayBuffer, 0, sizeof(displayBuffer));
	strcpy(displayBuffer, "HTTP/1.1 ");
	char *ext = getFileExtension(elem->req.fileName);
	//printf("HTTP Set \n");
	if (f != NULL) {
	// if(fd1 != -1){
		//printf("File Exists \n");
	//	fileExists = true;
		//ext = getFileExtension(elem->req.fileName);
//		printf("File Extension %s \n", ext);
		if (strcmp(ext, "") != 0) {
			//printf("Empty extension \n");
			if (strcmp(ext, "gif") == 0 || strcmp(ext, "jpeg") == 0
					|| strcmp(ext, "jpg") == 0) {
				//printf("Image Reply");

				strcat(displayBuffer, "200 OK\n");
				strcat(displayBuffer, "Content-Type:image/");
				strcat(displayBuffer, ext);
				strcat(displayBuffer,"\n");
			} else {
		//		printf("Extension %s \n",ext);
				//printf("Textual Reply \n");
				//printf("Textual Reply");
				strcat(displayBuffer, "200 OK\n");
				strcat(displayBuffer, "Content-Type:text/html\n");
		//		printf("content set to text html \n");
			}
		} else {
			strcat(displayBuffer, "200 OK\n");
			strcat(displayBuffer, "Content-Type:text/html\n");
		}
		strcat(displayBuffer,"Server:HTTP/1.0\n");
		strcat(displayBuffer,"Date:");
		strcat(displayBuffer,elem->req.processedTime);
		strcat(displayBuffer,"\n");
		strcat(displayBuffer, "Content-Length:");
		int fileSize = elem->req.fileSize;
		//printf("filesize %d \n", fileSize);

		char* fileSizeStr = NULL;
		int len = snprintf(NULL, 0, "%d", fileSize);
		//printf("Length %d", len);
	//	printf("Extension %s \n",ext);

		fileSizeStr = (char *) malloc(len);
		if (fileSizeStr != NULL) {
			//printf("FileSizeStr is not null");
			sprintf(fileSizeStr, "%d", fileSize);
			//printf("fileSize str %s", fileSizeStr);
			strcat(displayBuffer, fileSizeStr);
			free(fileSizeStr);
		}
		strcat(displayBuffer, "\n");
		strcat(displayBuffer, "Last-Modified:");
		char *date = (char *) malloc(29);
		if (date != NULL) {
			strftime(date, 29, "[%d/%b/%Y:%H:%M:%S %z]",
					gmtime(&(elem->req.modTime)));
			strcat(displayBuffer, date);
			free(date);
			//	printf("Date modified %s", date);
		}
	} else {
		printf("File does not exists \n");
		strcat(displayBuffer, "404 Not Found\n");
		strcat(displayBuffer, "Content-Type:text/html\n");
	}
	//printf("Extension %s \n",ext);
	//printf("Display Buffer %s \n", displayBuffer);
	strcat(displayBuffer, "\n\n");
	send(elem->req.acceptfd, displayBuffer, strlen(displayBuffer), 0);
	if (strcmp(elem->req.reqType, "GET") == 0) {
	//	printf("Get Request \n");
		if (f!= NULL) {
	//		printf("File is not null \n");
	//		printf("Extension %s",ext);
			if (strcmp(ext, "") == 0) {
				printf("Empty extension");
				DIR *d;
				struct dirent **dire;
				struct dirent *dir;

				d = opendir(elem->req.fullPath);
				if (d) {
					//printf("")
					write(elem->req.acceptfd,"<html><head><h1>Directory Listing</h1></head><body>",51);
					listings(elem);
				/*	while ((dir = readdir(d)) != NULL) {
						char *folder = (char*) malloc(strlen(dir->d_name) + 2);
						strcpy(folder, dir->d_name);
						strcat(folder, "<br>");
						write(elem->req.acceptfd, folder, strlen(folder));
						printf("%s", folder);
						free(folder);
					} */
					write(elem->req.acceptfd,"</body></html>",15);
					closedir(d);
				} else {
					write(elem->req.acceptfd, "No directory found",
							dir->d_name);
				}
			} else {
				printf("File with extension \n");
				char* buffer = (char *) malloc(elem->req.fileSize);
				//write(elem->req.acceptfd,"<HTML><HEAD></HEAD><BODY>",25);
				printf("%d \n",elem->req.fileSize);
				if (buffer != NULL) {
					fseek(f, 0L, SEEK_END);
					int size = ftell(f);
					fseek(f,0L,SEEK_SET);
					printf("size by fseek is %d \n",size);
					int a = fread(buffer, elem->req.fileSize, 1, f);
				//	int bufferlength = read(fd1, buffer, elem->req.fileSize);
				//	printf("Buffer Length %d",bufferlength);
				//	printf("Fread value %d",a);
					//	printf("Buffer %s \n", buffer);
				//	printf("buffer length %d",strlen(buffer));
					write(elem->req.acceptfd, buffer, strlen(buffer));
					elem->req.status = 200;
					elem->req.responseSize = strlen(buffer);
				//	printf("Send File Data \n");
				//	close(fd1);
					fclose(f);
					//printf("Buffer Size %s \n", buffer);
					//printf("Size %d \n",strlen(buffer));
					free(buffer);
					free(ext);
				}
				//write(elem->req.acceptfd,"</BODY></HTML>",13);
			}
			printf("File Read \n");
		} else {
			printf("File does not exist \n");
			char *buffer = (char *) malloc(60);
			if (buffer != NULL) {
				// send unable to server request
				strcpy(buffer, "ERROR 404 FILE NOT FOUND \n");
				printf("Buffer is %s \n", buffer);
				printf("Buffer size %d \n", strlen(buffer));
				printf("Acceptfd %d \n", elem->req.acceptfd);
				write(elem->req.acceptfd, buffer, strlen(buffer));
				elem->req.status = 200;
				elem->req.responseSize = strlen(buffer);
				free(buffer);
			}
		}
		printf("Free Extension \n");
	}
	printf("Close Connection \n");
	close(elem->req.acceptfd);

	/*	if (f != NULL) {
	 char *buffer = (char *) malloc(sizeof(elem->req.fileSize));
	 if (buffer == NULL) {
	 // send unable to server request
	 }

	 /*	char displayBuffer[BUFFSIZE];
	 printf("Memset to be done \n");
	 memset(displayBuffer, 0, sizeof(displayBuffer));

	 printf("pos obtained %s \n", ext);
	 printf("filename %s \n",elem->req.fileName );

	 printf("Last modified time: %s \n", date);
	 //	printf("Display Buffer %s \n", displayBuffer);
	 //printf("Success copied \n");
	 //	printf("String length %d \n", strlen(displayBuffer));
	 send(elem->req.acceptfd, displayBuffer, strlen(displayBuffer), 0);
	 fread(buffer, elem->req.fileSize, 1, f);
	 //	printf("Buffer %s \n", buffer);
	 //	printf("buffer length %d",strlen(buffer));
	 send(elem->req.acceptfd, buffer, strlen(buffer), 0);
	 printf("Send File Data \n");
	 close(elem->req.acceptfd); */
	//}
}

void listings(Element *elem){
	printf("Entered listing \n");
	struct dirent **dirList;
	printf("Going to scan dir \n");
	int count = scandir(elem->req.fullPath, &dirList, NULL, alphasort);
	printf("Count is %d \n",count);
	int i =0;
	while(i < count){
		char *folder = (char*) malloc(strlen(dirList[i]->d_name));
		strcpy(folder, dirList[i]->d_name);
		strcat(folder, "<br>");
		write(elem->req.acceptfd, folder, strlen(folder));
		free(folder);
		free(dirList[i]);
		i++;
	}
	free(dirList);
}

char* getFileExtension(const char* fileName) {
	printf("Entered getFile Extenstion \n");
	const char *dot = strrchr(fileName, '.');
	if (!dot || dot == fileName) {
		return "";
	} else {
		char *ext = (char *)malloc(strlen(dot));
		printf("Length is %d",strlen(dot));
		strcpy(ext,dot+1);
		strcat(ext,"\0");
		printf("Extension is %s",ext);
		return ext;
	}
}

void logCurrentRequest(Element *elem) {

	if (logging && !debug) {
		logFile = fopen(loggerFile,"a");
		if (logFile != NULL) {
			printf("Log File is present \n");
			fprintf(logFile, "%s %s %s \"%s\" %d %d\n", elem->req.ipAddress,
					elem->req.arrivalTime, elem->req.processedTime,
					elem->req.firstLine, elem->req.status,
					elem->req.responseSize);
			fclose(logFile);
		}
	}else{
		printf("%s %s %s \"%s\" %d %d\n", elem->req.ipAddress,
							elem->req.arrivalTime, elem->req.processedTime,
							elem->req.firstLine, elem->req.status,
							elem->req.responseSize);
	}
	/*	 */
}
