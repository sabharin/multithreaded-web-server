# include "Server.h"
# include "RequestProcessor.h"

struct addrinfo inValue, *serverInfo, *validInfo;
struct sockaddr_storage clientAddr;
int acceptId, address,yes;
char ipAddress[INET6_ADDRSTRLEN];
char incomingBuffer[BUFFSIZE];
socklen_t addrlen;

void *get_ip_address(struct sockaddr *s);
u_int16_t get_port_number(struct sockaddr *s);

u_int16_t get_port_number(struct sockaddr *s)
{
	if(s->sa_family == AF_INET)
		return (((struct sockaddr_in  *)s)->sin_port);
	else
		return (((struct sockaddr_in6 *)s)->sin6_port);
}

void* get_ip_address(struct sockaddr *s)
{
	if(s->sa_family == AF_INET)
		return &((struct sockaddr_in *)s)->sin_addr;
	else
		return &((struct sockaddr_in6 *)s)->sin6_addr;
}

char* convertNumToString(int number){

	char* charstr = NULL;
	int len = snprintf(NULL, 0, "%d", number);
	charstr = (char *)malloc(len);
	sprintf(charstr,"%d",number);

	return charstr;

}

char* getCurrentTimeAsStr(){


	char* result= NULL;

	time_t currTime;
	time(&currTime);

	struct tm *now = (struct tm*)gmtime(&currTime);

	result = (char *)malloc(29 *sizeof(char));

	strftime(result,29,"[%d/%b/%Y:%H:%M:%S %z]", now);

	return result;
}

void startServer(int* portnum){

	printf("started server;;\n");

    printf("Port number :: %d",*portnum);
	char* portinstr = convertNumToString(*portnum);
	int on = 0;
	memset(&inValue, 0, sizeof(inValue));
	inValue.ai_family = AF_UNSPEC;
	inValue.ai_socktype = SOCK_STREAM;
	inValue.ai_flags = AI_PASSIVE;

	if (getaddrinfo(NULL, portinstr, &inValue, &serverInfo) != 0)
		perror("Get Address:"); 

	printf(" Starting a Server \n");

	printf("Socket creation completed \n");

	if((sockfd = (socket(serverInfo->ai_family, serverInfo->ai_socktype,0))) == -1)
		perror("Socket:");

	printf("Socket Created \n");

	addrlen = sizeof(serverInfo);

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1){
		perror("setsockopt");
		exit(1);
	}

	printf("Socket option set \n");

	if(bind(sockfd,serverInfo->ai_addr, serverInfo->ai_addrlen) == -1){
		perror("Bind:");
		exit(1);
	}

	printf("Bind completed \n");

	freeaddrinfo(serverInfo); 

	if(listen(sockfd, MAXCONN) == -1)
		perror("Listen:");

	address = sizeof(clientAddr);
	printf("listener thread about to start \n");

	while(true) {		// Main thread will listen continously 

        printf("Listener is running \n");
		if((acceptId = accept(sockfd,(struct sockaddr*)&clientAddr,(socklen_t *)&address)) == -1)
			perror("Accept:");

		char* currTime = getCurrentTimeAsStr();

		fcntl( acceptId, O_NONBLOCK, 0 );

		inet_ntop(clientAddr.ss_family,get_ip_address((struct sockaddr *)&clientAddr),ipAddress, sizeof(ipAddress));
		u_int16_t clientPort = get_port_number((struct sockaddr *)&(serverInfo));
		processRequest(acceptId,ipAddress,incomingBuffer,currTime);

	}
}
