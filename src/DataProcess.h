#define SUCCESS_TEXT     "HTTP/1.0 200 OK\nContent-Type:text/html\n\n"
#define FAILURE_TEXT   "HTTP/1.0 404 Not Found\nContent-Type:text/html\n\n"
#define FAILURE_CONTENT    "<html><body><h1>FILE NOT FOUND</h1></body></html>"
