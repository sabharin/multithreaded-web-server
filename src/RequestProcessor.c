# include "myhttpd.h"
# include "RequestProcessor.h"
# include <pwd.h>
#include <stdio.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/stat.h>

struct request {
	int acceptfd;
	int fileSize;
	char* ipAddress;
	char* reqType;
	char* fileName;
	char* fullPath;
	bool isRootDir;
	char* arrivalTime;
	char *processedTime;
	char* firstLine;
	int status;
	int responseSize;
	time_t modTime;
};

struct node {
	struct request req;
	struct node *next;
}*head = NULL, *tail = NULL, *nextRequest = NULL;

static int threadid = 0;

char incomingBuffer[BUFFSIZE];

char tempBuf[BUFFSIZE];

void insertToQueue(Element *elem);

void print();

bool checkRootFolder(char* fileName);

char* getFullPath(char *buffer);

void insertRequest(int acceptfd, char* ipAddress, char* reqType, char* fileName,
		char* firstLine, char* arrivalTime);

char* getFileExtension(const char* fileName);

void processRequest(int acceptfd, char* ipAddress, char* incomingBuffer1,
		char* arrivalTime) {

	int retcode = 0;
	memset(incomingBuffer, 0, sizeof(tempBuf));
	if ((retcode = recv(acceptfd, tempBuf, sizeof(tempBuf), 0)) < 0) {
		perror("Receive Error");
	}

	strcpy(incomingBuffer, tempBuf);

	char* firstLine = strtok(incomingBuffer, "\r\n");

	int len = snprintf(NULL, 0, "%s", firstLine);
	char* elemFirstLine = (char *) malloc((len + 1) * sizeof(char));
	snprintf(elemFirstLine, (len + 1), "%s", firstLine);

	char* firstToken = strtok(firstLine, " ");
	len = snprintf(NULL, 0, "%s", firstToken);
	char* reqType = (char *) malloc((len + 1) * sizeof(char));
	snprintf(reqType, (len + 1), "%s", firstToken);

	char* secondToken = strtok(NULL, " ");
	char* fileName = NULL;
	if(strcmp(secondToken,"/") != 0)
	{
		len = snprintf(NULL, 0, "%s", secondToken);
		fileName = (char *) malloc((len + 1) * sizeof(char));
		snprintf(fileName, (len + 1), "%s", secondToken);
	}else{
		fileName = (char *) malloc(11);
		strcpy(fileName,"/index.html");
	}

	insertRequest(acceptfd, ipAddress, reqType, fileName, elemFirstLine,
			arrivalTime);

	firstLine = NULL;
	firstToken = NULL;
	secondToken = NULL;
}

void processFileInfo(char* fileName, Element *elem) {

	bool isRootFolder = checkRootFolder(fileName);
	char *fileDup = (char *) malloc(strlen(fileName));
	fileDup = strdup(fileName);
	char *root = getFullPath(fileDup);
	elem->req.isRootDir = isRootFolder;
	elem->req.fullPath = root;
	free(fileDup);
}

bool checkRootFolder(char* fileName) {

	char *dup = (char *) malloc(strlen(fileName));
	dup = strdup(fileName);
	char *pos = strchr(dup, '/');
	if (pos != NULL) {
		int firstOccurence = pos - dup + 1;

		*pos = strchr(dup, '/');

		if (pos == NULL) {
			free(dup);
			return true;
		}
	}
	free(dup);
	return false;
}

char *replaceStr(char *str, char *strToRep, char *userString) {
	char buffer[4096];
	char *newString = NULL;
	char *p;

	if (!(p = strstr(str, strToRep)))
		return str;

	strncpy(buffer, str, p - str); 
	buffer[p - str] = '\0';

	sprintf(buffer + (p - str), "%s%s", userString, p + strlen(strToRep));

	newString = (char *) malloc(strlen(buffer));

	strcpy(newString, buffer);

	return newString;
}

char* getFullPath(char *buffer) {
	char *val = (char *) malloc(strlen(buffer));
	strcpy(val, buffer);
	char *pos = strchr(val, '~');

	char *newString = NULL;
	char *userRoot = NULL;
	char *fullFilePath = NULL;
	if ((pos != NULL) && ((pos - val + 1) == 2)) {
		newString = strtok(pos + 1, "/");

		struct passwd *userChange= getpwnam(newString);
		if(userChange != NULL){
			char* userHome = malloc(sizeof((userChange->pw_dir)));
			strcpy(userHome,userChange->pw_dir);
			userRoot = replaceStr("<user>/myhttpd/", "<user>", userHome);
		}else{
			printf("User does not exists \n");
			userRoot = replaceStr("/home/<user>/myhttpd/", "<user>", newString);
		}

		newString = strtok(NULL, "/");
		printf("new string is %s",newString);
		if(newString == NULL){
				char *indexPath = (char *)malloc(strlen(userRoot));
				strcpy(indexPath,userRoot);
				userRoot = (char *)realloc(userRoot,strlen(indexPath) + 10);
				strcpy(userRoot,indexPath);
				strcat(userRoot,"index.html");
		}
	} else {
		userRoot = (char *) malloc(strlen(rootDir) + 6);
		strcpy(userRoot, rootDir);
		newString = strtok(val, "/");
	}

	char *path = NULL;
	while (newString != NULL) {
		if (newString != NULL) {
			if (path == NULL) {
				path = (char *) malloc(strlen(newString));
				strcpy(path, newString);
			} else {
				path = (char *) realloc(path,
						strlen(path) + strlen("/") + strlen(newString));
				strcat(path, "/");
				strcat(path, newString);
			}
		}
		newString = strtok(NULL, "/");
	}

	if (path == NULL) {
		fullFilePath = (char *) malloc(strlen(userRoot));
		strcpy(fullFilePath, userRoot);
	} else {
		fullFilePath = (char *) malloc(strlen(userRoot) + strlen(path));
		strcpy(fullFilePath, userRoot);
		strcat(fullFilePath, path);
	}
	
	if(userRoot!=NULL){
	free(userRoot);
	}
	if(path!=NULL){
	free(path);
	}
	if(val != NULL){
	free(val);
	}

	return fullFilePath;
}

void insertRequest(int acceptfd, char* ipAddress, char* reqType, char* fileName,
		char* firstLine, char* arrivalTime) {

	Element* elem = (Element *) malloc(sizeof(struct node));

	elem->req.acceptfd = acceptfd;

	int len = snprintf(NULL, 0, "%s", ipAddress);
	elem->req.ipAddress = (char *) malloc((len + 1) * sizeof(char));
	snprintf(elem->req.ipAddress, (len + 1), "%s", ipAddress);

	elem->req.reqType = reqType;

	elem->req.fileName = fileName;

	processFileInfo(fileName, elem);

	int filesize = 0;

	struct stat fileInfo;
	printf("Full path %s \n", elem->req.fullPath);
	if (stat(elem->req.fullPath, &fileInfo) == 0) {
		filesize = fileInfo.st_size;
	}

	elem->req.modTime = fileInfo.st_mtime;

	elem->req.fileSize = filesize;

	printf("Firstline %s \n", firstLine);

	elem->req.firstLine = firstLine;

	elem->req.arrivalTime = arrivalTime;

	elem->next = NULL;
	printf(" Element to be inserted \n");

	printf(
			" Acceptfd %d \n "/*, IpAddress %s \n, ReqType %s \n, FileName %s \n ,FileSize %d \n, FirstLine %s \n, ArrivalTime %s \n"*/,
			elem->req.acceptfd/*, elem->req.ipAddress, elem->req.reqType,
			 elem->req.fileName, elem->req.fileSize, elem->req.firstLine,
			 elem->req.arrivalTime*/);

	// lock the queue

	pthread_mutex_lock(&reqQueueLock);
	printf("Inserting an element \n");
	insertToQueue(elem);
	printf("\n Signalling insertion \n");
	pthread_cond_signal(&queueCond);
	pthread_mutex_unlock(&reqQueueLock);
	// unlock the queue 
	// signal 

	print();
}

void insertToQueue(Element *elem) {

	if (head == NULL) {
		head = elem;
		tail = elem;
	} else {
		if (schedulerType == 0) {
			tail->next = elem;
			tail = elem;
		} else {
			// Iterate through the list till file size > elem's file size

			Element* temp = NULL;
			temp = head;

			int size = elem->req.fileSize;
			int currentSize = temp->req.fileSize;


			if (currentSize > size) {
				elem->next = head;
				head = elem;
			} else {
				if (temp->next != NULL) {
					while (true) {

						int currentSize = temp->next->req.fileSize;

						if (currentSize <= size) {
							if (temp->next->next == NULL) {
								break;
							} else {
								temp = temp->next;
							}
						} else {
							break;
						}
					}

					if (temp->next->next == NULL) {
						temp->next->next = elem;
					} else {
						elem->next = temp->next;
						temp->next = elem;
					}

				} else {
					temp->next = elem;
				}
			}
		}
	}
}

void scheduler() {
	printf("Scheduler Execution started \n");
	while (true) {
		printf("Waiting for threadSemaphore \n");

		sem_wait(&threadSem);
		pthread_mutex_lock(&reqQueueLock);
		while (head == NULL) {
			printf("Waiting for user request \n");
			pthread_cond_wait(&queueCond, &reqQueueLock);
		}

		pthread_mutex_lock(&reqProcessorLock);
		printf("Retrieving First Request \n");
		getFirstRequest();
		printf("Retrieved the first Request \n");
		pthread_cond_signal(&nextRequestCond);
		pthread_mutex_unlock(&reqProcessorLock);
		pthread_mutex_unlock(&reqQueueLock);
		while (nextRequest != NULL) {
			sleep(1);
		}
	}
}

void getFirstRequest() {
	nextRequest = head;
	head = head->next;
}

void runner() {
	int a = ++threadid;
	sem_post(&threadSem);
	while (true) {

		// Lock for multiple threads
		pthread_mutex_lock(&reqProcessorLock);

		// Wait for request to be filled
		printf("Thread id %d \n", a);
		printf("Waiting for Scheduler \n");
		pthread_cond_wait(&nextRequestCond, &reqProcessorLock);
		printf("Request Available!!!! \n");
		Element* currentRequest = nextRequest;

		nextRequest = NULL;

		pthread_mutex_unlock(&reqProcessorLock);

		printf("Processing the request acceptfd %d \n",
				currentRequest->req.acceptfd);

		currentRequest->req.processedTime = getCurrentTimeAsStr();
		time_t currTime;
		time(&currTime);
		struct tm *now = (struct tm*) gmtime(&currTime);
		currentRequest->req.processedTime = (char *) malloc(29 * sizeof(char));
		strftime(currentRequest->req.processedTime, 29,
				"[%d/%b/%Y:%H:%M:%S %z]", now);
		processAndSendData(currentRequest);
		pthread_mutex_lock(&loggerLock);
		logCurrentRequest(currentRequest);
		pthread_mutex_unlock(&loggerLock);
		free(currentRequest);
		printf("the request acceptfd %d \n", currentRequest->req.acceptfd);
		printf("clearing current request \n");
		// Unlock for threads
		sem_post(&threadSem);
		printf("A thread is free now");
	}
}

void print() {

	Element* temp = NULL;
	if (head == NULL) {
		printf("Queue is empty !!\n");
	} else {
		temp = head;
		printf("Queue Print \n");
		while (temp != NULL) {
			printf(" Acceptfd %d \t File Size %d \n",/* , IpAddress %s \n, ReqType %s \n, FileName %s \n, FirstLine %s \n, ArrivalTime %s \n", */
			temp->req.acceptfd,
					temp->req.fileSize /*, temp->req.ipAddress, temp->req.reqType , temp->req.fileName , temp->req.firstLine , temp->req.arrivalTime*/);
			temp = temp->next;
		}
	}
}

void processAndSendData(Element* elem) {

	FILE* f = fopen(elem->req.fullPath, "r");
	char displayBuffer[BUFFSIZE];
	memset(displayBuffer, 0, sizeof(displayBuffer));
	strcpy(displayBuffer, "HTTP/1.1 ");
	char *ext = getFileExtension(elem->req.fileName);
	if (f != NULL) {
		if (strcmp(ext, "") != 0) {
			if (strcmp(ext, "gif") == 0 || strcmp(ext, "jpeg") == 0
					|| strcmp(ext, "jpg") == 0) {

				strcat(displayBuffer, "200 OK\n");
				strcat(displayBuffer, "Content-Type:image/");
				strcat(displayBuffer, ext);
				strcat(displayBuffer,"\n");
			} else {
				strcat(displayBuffer, "200 OK\n");
				strcat(displayBuffer, "Content-Type:text/html\n");
			}
		} else {
			strcat(displayBuffer, "200 OK\n");
			strcat(displayBuffer, "Content-Type:text/html\n");
		}
		strcat(displayBuffer,"Server:HTTP/1.0\n");
		strcat(displayBuffer,"Date:");
		strcat(displayBuffer,elem->req.processedTime);
		strcat(displayBuffer,"\n");
		strcat(displayBuffer, "Content-Length:");
		int fileSize = elem->req.fileSize;

		char* fileSizeStr = NULL;
		int len = snprintf(NULL, 0, "%d", fileSize);

		fileSizeStr = (char *) malloc(len);
		if (fileSizeStr != NULL) {
			sprintf(fileSizeStr, "%d", fileSize);
			strcat(displayBuffer, fileSizeStr);
			free(fileSizeStr);
		}
		strcat(displayBuffer, "\n");
		strcat(displayBuffer, "Last-Modified:");
		char *date = (char *) malloc(29);
		if (date != NULL) {
			strftime(date, 29, "[%d/%b/%Y:%H:%M:%S %z]",
					gmtime(&(elem->req.modTime)));
			strcat(displayBuffer, date);
			free(date);
		}
	} else {
		printf("File does not exists \n");
		strcat(displayBuffer, "404 Not Found\n");
		strcat(displayBuffer, "Content-Type:text/html\n");
	}
	strcat(displayBuffer, "\n\n");
	send(elem->req.acceptfd, displayBuffer, strlen(displayBuffer), 0);
	if (strcmp(elem->req.reqType, "GET") == 0) {
		if (f!= NULL) {
			if (strcmp(ext, "") == 0) {
				printf("Empty extension");
				DIR *d;
				struct dirent **dire;
				struct dirent *dir;

				d = opendir(elem->req.fullPath);
				if (d) {
					write(elem->req.acceptfd,"<html><head><h1>Directory Listing</h1></head><body>",51);
					listings(elem);
					write(elem->req.acceptfd,"</body></html>",15);
					closedir(d);
				} else {
					write(elem->req.acceptfd, "No directory found",
							dir->d_name);
				}
			} else {
				printf("File with extension \n");
				char* buffer = (char *) malloc(elem->req.fileSize);
				printf("%d \n",elem->req.fileSize);
				if (buffer != NULL) {
					fseek(f, 0L, SEEK_END);
					int size = ftell(f);
					fseek(f,0L,SEEK_SET);
					printf("size by fseek is %d \n",size);
					int a = fread(buffer, elem->req.fileSize, 1, f);
					write(elem->req.acceptfd, buffer, strlen(buffer));
					elem->req.status = 200;
					elem->req.responseSize = strlen(buffer);
					fclose(f);
					free(buffer);
					free(ext);
				}
			}
			printf("File Read \n");
		} else {
			printf("File does not exist \n");
			char *buffer = (char *) malloc(60);
			if (buffer != NULL) {
				// send unable to server request
				strcpy(buffer, "ERROR 404 FILE NOT FOUND \n");
				printf("Buffer is %s \n", buffer);
				printf("Buffer size %d \n", strlen(buffer));
				printf("Acceptfd %d \n", elem->req.acceptfd);
				write(elem->req.acceptfd, buffer, strlen(buffer));
				elem->req.status = 200;
				elem->req.responseSize = strlen(buffer);
				free(buffer);
			}
		}
		printf("Free Extension \n");
	}
	printf("Close Connection \n");
	close(elem->req.acceptfd);
}

void listings(Element *elem){
	printf("Entered listing \n");
	struct dirent **dirList;
	printf("Going to scan dir \n");
	int count = scandir(elem->req.fullPath, &dirList, NULL, alphasort);
	printf("Count is %d \n",count);
	int i =0;
	while(i < count){
		char *folder = (char*) malloc(strlen(dirList[i]->d_name));
		strcpy(folder, dirList[i]->d_name);
		strcat(folder, "<br>");
		write(elem->req.acceptfd, folder, strlen(folder));
		free(folder);
		free(dirList[i]);
		i++;
	}
	free(dirList);
}

char* getFileExtension(const char* fileName) {
	printf("Entered getFile Extenstion \n");
	const char *dot = strrchr(fileName, '.');
	if (!dot || dot == fileName) {
		return "";
	} else {
		char *ext = (char *)malloc(strlen(dot));
		printf("Length is %d",strlen(dot));
		strcpy(ext,dot+1);
		strcat(ext,"\0");
		printf("Extension is %s",ext);
		return ext;
	}
}

void logCurrentRequest(Element *elem) {

	if (logging && !debug) {
		logFile = fopen(loggerFile,"a");
		if (logFile != NULL) {
			printf("Log File is present \n");
			fprintf(logFile, "%s %s %s \"%s\" %d %d\n", elem->req.ipAddress,
					elem->req.arrivalTime, elem->req.processedTime,
					elem->req.firstLine, elem->req.status,
					elem->req.responseSize);
			fclose(logFile);
		}
	}else{
		printf("%s %s %s \"%s\" %d %d\n", elem->req.ipAddress,
							elem->req.arrivalTime, elem->req.processedTime,
							elem->req.firstLine, elem->req.status,
							elem->req.responseSize);
	}
	/*	 */
}
