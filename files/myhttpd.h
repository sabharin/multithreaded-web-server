#ifndef MYHTTPD_H_
#define MYHTTPD_H_

# include <pthread.h>
# include <semaphore.h>
# include <stdbool.h>
# include <stdio.h>

extern pthread_mutex_t reqQueueLock;
extern pthread_mutex_t reqProcessorLock;
extern pthread_mutex_t loggerLock;
extern pthread_cond_t nextRequestCond;
extern pthread_cond_t queueCond;
extern FILE *logFile;
extern sem_t threadSem;
extern bool debug;// = 0;
extern bool help;// = 0;
extern bool logging;// = 0;
extern char *loggerFile;// = "requests.log";
extern int port;// = 8080;
extern bool dir;// = 0;
extern char *rootDir;// = "/home/sabhari/project/";
extern int queueTime;// = 60;
extern int numThreads;// = 4;
extern int option;// = 0;
extern int schedulerType;// = 0;

void printHelp();
bool convertStringToNum(int *value, char *arg);

#endif
